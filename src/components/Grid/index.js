// @flow
import React from 'react';
import styled from 'styled-components';
import { Table, TableHead, TableRow, TableBody, TableCell, Paper } from '@material-ui/core';
import uuid from 'uuid';

interface GridProps {
    data: Array;
    columns: Array;
    gridStyle: any;
}

export default function Grid(props: GridProps) {
    const { data, columns, gridStyle } = props;
    return (
        <Paper style={{ overflowX: 'auto', width: '100%' }}>
            <Table style={{ minWidth: '700px' }}>
                <TableHead>
                    <TableRow>
                        {columns.map(column => {
                            let styles;
                            styles = {...styles, ...gridStyle.header}
                            if(column.style){
                                if(column.style.column){
                                    styles = {...styles, ...column.style.column }; 
                                }
                                if(column.style.header){
                                    styles = {...styles, ...column.style.header };
                                }
                            }
                            return <TableCell style={styles} key={uuid.v4()}>{column.header ? column.header : ''}</TableCell>
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map(item => (
                        <TableRow key={uuid.v4()}>
                            {columns.map((column, index) => {
                                let cell = '';
                                if (column.field) {
                                    cell = item[column.field];
                                } else if (column.component) {
                                    cell = column.component(item);
                                }
                                let styles = {};
                                if(column.style){
                                    if(column.style.column){
                                        styles = {...styles, ...column.style.column }; 
                                    }
                                }
                                return <TableCell style={{ ...gridStyle.rows, ...styles }} key={uuid.v4()}>{cell}</TableCell>
                            })}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    )
}