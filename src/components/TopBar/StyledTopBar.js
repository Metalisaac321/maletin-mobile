import styled from 'styled-components';

const StyledTopBar = styled.div`
    display: grid;
    grid-template-columns: 10% 75% 15%;
    height: 3em;
    padding: 5px;   
    align-items: center;
    color: ${(props) => props.blue? 'white': '#4F4F4F;'}
    background-color: ${(props) => props.blue? '#007CEE;' : '#F8F8F8'};
    box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12);
    margin-bottom: 15px;
`;

export default StyledTopBar;