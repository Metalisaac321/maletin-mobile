// @flow
import React from 'react';
import { IconButton, Avatar } from '@material-ui/core';
import StyledTopBar from './StyledTopBar';
import styled from 'styled-components';

interface TopBarProps {
    onClickIcon: () => void;
    blue?: boolean;
    icon: any;
    titulo: string;
    avatarUrl: string;
}

const TopBarTitle = styled.div`
    font-size: 18px;
    font-family: Helvetica;
    margin-left: 5px;
`;

export default function TopBar(props: TopBarProps) {
    return (
        <StyledTopBar blue={props.blue}>
            <IconButton onClick={props.onClickIcon} color="inherit" aria-label="Menu">
                {props.icon}
            </IconButton>
            <TopBarTitle>{props.titulo}</TopBarTitle>
            <Avatar alt="usuario" src={props.avatarUrl} style={{justifySelf: 'end', marginRight:'0.5em'}}/>
        </StyledTopBar>
    );
}