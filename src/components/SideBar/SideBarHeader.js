// @flow
import React from 'react';
import styled from 'styled-components';
import ArrowBack from '@material-ui/icons/ArrowBack';
import { IconButton } from '@material-ui/core';

const StyledSideBarHeader = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);;
    background-color: #F8F8F8;
    font-family: 'Open Sans'; 
    font-weight: bold;
`;

interface SideBarHeaderProps {
    titulo: string;
    onClickIconoTituloMenu: () => void;
}

export default function SideBarHeader(props: SideBarHeaderProps) {
    return (
        <StyledSideBarHeader>
            <IconButton color="inherit" onClick={props.onClickIconoTituloMenu}>
                <ArrowBack />
            </IconButton>
            {props.titulo}
        </StyledSideBarHeader>
    );
}
