// @flow
import React from 'react';
import SideBarItem from './SideBarItem';
import uuid from 'uuid';

interface SideBarItemListProps {
    opcionesMenu: Array<string>;
    opcionActiva: string;
    onClickOpcionMenu: (opcionSeleccionada: string) => void;
}

export default function SideBarMenuItemList(props: SideBarItemListProps) {
    const { opcionesMenu, onClickOpcionMenu, opcionActiva } = props;

    return (
        opcionesMenu.map<SideBarItem>((opcion: string) => opcionActiva === opcion ? 
            <SideBarItem active onClick={() => onClickOpcionMenu(opcion)} key={uuid.v4()}>{ opcion } </SideBarItem> : 
            <SideBarItem onClick={() => onClickOpcionMenu(opcion)} key={uuid.v4()}>{ opcion } </SideBarItem>
        )
    );
}