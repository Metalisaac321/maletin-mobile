// @flow
import styled from 'styled-components';

const SideBarItem = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    padding: 6% 12%;
    cursor: pointer;
    font-family: 'Open Sans'; 
    color: #212121;
    font-size: 18px;
    ${props => props.active ? `
        border-left: 6px solid #FFAB00;
        color: #FFAB00;` : ``
    }
`;

export default SideBarItem;