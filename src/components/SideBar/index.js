// @flow
import React from 'react';
import SideBarHeader from './SideBarHeader';
import SideBarMenuItemList from './SideBarMenuItemList';
import { push as Menu } from 'react-burger-menu'

interface SideBarProps {
    titulo: string;
    opcionesMenu: Array<string>;
    opcionActiva: string;
    idPagina: string;
    isOpen: boolean;
    onClickOpcionMenu: (opcionSeleccionada: string) => void;
    onClickIconoTituloMenu?: () => void;
}

export default function SideBar(props: SideBarProps) {
    const { titulo, opcionesMenu, onClickOpcionMenu, opcionActiva, idPagina, isOpen, onClickIconoTituloMenu} = props;
    return (
        <Menu isOpen={ isOpen } pageWrapId={ idPagina } width={ '75%' } styles={{bmMenu: {background: 'white'}}}>
            <SideBarHeader {...{ titulo, onClickIconoTituloMenu }}/>
            <SideBarMenuItemList {...{ opcionesMenu, opcionActiva, onClickOpcionMenu } } />
        </Menu>
    );
}