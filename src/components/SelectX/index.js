import React from 'react';
import { Select, MenuItem, withStyles } from '@material-ui/core';
import uuid from 'uuid';
import InputBase from '@material-ui/core/InputBase';

interface SelectXProps {
    items: Array<T>;
    onSelectChange: (itemSeleccionado: T) => void;
    actualSelectItem: T;
    showTextProp: string;
}

const BootstrapInput = withStyles(theme => ({
    root: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    input: {
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        width: 'auto',
        padding: '10px 26px 10px 12px',
        minWidth: '60px'
    },
  }))(InputBase);
  
export default function SelectX(props: SelectXProps) {
    const { items, onSelectChange, actualSelectItem, showTextProp } = props;
    let actualValue = JSON.stringify(actualSelectItem);
    
    if(items.length > 0 && !items[0][showTextProp]) {
        throw new Error(`No existe la propiedad ${showTextProp} en los objetos de los items`);
    }

    return (
        <Select 
            value={actualValue} 
            onChange={(event) => onSelectChange(JSON.parse(event.target.value))}
            input={<BootstrapInput />}
            >
            <MenuItem value="{}"></MenuItem>
            {props.items.map(item => 
                <MenuItem key={uuid.v4()} value={JSON.stringify(item)}>{item[showTextProp]}</MenuItem>
            )}
        </Select>
    );
};