import React, { Component } from 'react';
import GlobalStyle from './globalStyles';
import SideBar from './components/SideBar';
import TopBar from './components/TopBar';
import MenuIcon from '@material-ui/icons/Menu';
import styled from 'styled-components';
import SelectX from './components/SelectX';
import AppContent from './components/AppContent';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import Grid from './components/Grid';

const ContentWrapper = styled.div`

`;

const MultiColorText = styled.div`
  color: ${props => props.color};
`;

@observer
class App extends Component {
  @observable store = {
    openMenu: false,
    opcionActiva: '',
    actualSelectItem: {}
  }

  toggleMenu = () => {
    this.store.openMenu = !this.store.openMenu;
  }

  onClickOpcionMenu = (opcionSeleccionada) => {
    this.store.opcionActiva = opcionSeleccionada;
  }

  onClickIconoTituloMenu = () => {
    this.store.openMenu = !this.store.openMenu;
  }

  onSelectChange = (itemSeleccionado) => {
    this.store.actualSelectItem = itemSeleccionado;
  }

  onClick = (event, row) => {
    console.log('event', event);
    console.log('row', row);
  }

  onClickBorrar(event, row) { 
    console.log('ha borrar se ha dicho');
  }
  onClickEditar(event, row){
    console.log('ha editar se ha dicho');
  }
  onClickDetalles(event, row){
    console.log('hay que ver los detalles');
  }

  render() {
    const { opcionActiva, actualSelectItem } = this.store;
    const onClickOpcionMenu = this.onClickOpcionMenu;
    const onClickIconoTituloMenu = this.onClickIconoTituloMenu;
    const onSelectChange = this.onSelectChange;
    let opcionesMenu = ['Asistencias', 'Participaciones', 'Alumnos', 'Tareas',
      'Trabajo en clase', 'Evaluaciones', 'Material'];
    let titulo = 'Grupos';

    let idPagina = "page-wrap";
    let isOpen = this.store.openMenu;

    let data = [
      { id: 1, nombre: 'Periodo 1', color: 'red' },
      { id: 2, nombre: 'Periodo 2', color: 'green' },
      { id: 3, nombre: 'Periodo 99', color: 'gray' }
    ];

    let columns = [
      {
        header: 'Id', field: 'id',
        style: {
          column: {
            paddingRight: '0'
          }
        }
      },
      {
        header: 'Nombre', field: 'nombre'
      },
      {
        header: 'Color', component: (row) =>
          <MultiColorText color={row.color}>
            {row.color}
          </MultiColorText>
      },
      {
        header: 'Funcionalidades chiloria', component: (row) => {
          return (
            <>
              <button onClick={(event) => this.onClickBorrar(event, row)}>Borrar</button>
              <button onClick={(event) => this.onClickEditar(event, row)}>Editar</button>
              <button onClick={(event) => this.onClickDetalles(event, row)}>Detalles</button>
            </>
          )
        },
        style: {
          column: {
            textAlign: 'center',
            backgroundColor: 'red'
          },
          header: {
            backgroundColor: 'purple'
          }
        },
      }
    ];

    let gridStyle = {
      header: {
        color: '#fff',
        backgroundColor: '#000',
        fontSize: '16px',
        borderBottom: '5px solid red'
      },
      rows: {
        backgroundColor: '#00800033'
      }
    }

    return (
      <div>
        <ContentWrapper id={idPagina}>
          <TopBar
            blue
            icon={<MenuIcon />}
            onClickIcon={this.toggleMenu}
            titulo={"1° A"}
            avatarUrl={"http://globalmediarocks.s3-us-west-1.amazonaws.com/uploads/8481f32f-045e-4ca6-92d1-6125b5acad43/amlo_viernes_24_65690334.jpg"} />
          <AppContent>
            <Grid data={data} columns={columns} gridStyle={gridStyle} />
          </AppContent>
        </ContentWrapper>
        <GlobalStyle />
      </div>
    );

  }
}

export default App;
